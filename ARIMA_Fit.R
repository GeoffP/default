############################################
### Process To Download Historical Data  ###
### For Analysis                         ###
###                                      ###
############################################

library(quantmod)
library(ggplot2)
library(tseries)
library(forecast)

setwd("~/Documents/default/")

tickers <- c("PUTW", "SPY")
sdate <- "2016-02-24"
edate <- Sys.Date()

getSymbols(Symbols = tickers, src = "tiingo", api.key = Sys.getenv("RIINGO_TOKEN"),
           periodicity = "daily", adjust = TRUE, from = sdate, to = edate)

ggplot(PUTW, aes(x = Index, y = PUTW.Close)) +
  geom_line() +
  ylab("PUTW Daily Closing Price") +
  xlab("")

ggplot(SPY, aes(x = Index, y = SPY.Close)) +
  geom_line() +
  ylab("SPY Daily Closing Price") +
  xlab("")

spy_log_returns <- c(na.omit(diff(log(SPY$SPY.Close), lag = 1)))
putw_log_returns <- c(na.omit(diff(log(PUTW$PUTW.Close), lag = 1)))

# Test for Stationarity and Find ACF and PACF of Time-Series
adf.test(putw_log_returns$PUTW.Close)
Acf(putw_log_returns$PUTW.Close, main = "PUTW Daily Log ACF")
Pacf(putw_log_returns$PUTW.Close, main = "PUTW Daily Log PACF")







