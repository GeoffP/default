library("lmtest")
library("AER")
data("USMoney")

## Test if M1 Money Supply Causes GNP
grangertest(gnp ~ m1, order = 3, data = USMoney)

## Test if GNP causes M1 Money Supply
grangertest(m1 ~ gnp, order = 3, data = USMoney)

## Alternative Inputs Which Are The "Same" As Test Just Performed
grangertest(USMoney[,1], USMoney[,2], order = 3)

## 3.5.7 Lab
data("ChickEgg")
grangertest(egg ~ chicken, order = 3, data = ChickEgg)
grangertest(chicken ~ egg, order = 3, data = ChickEgg)