library(fBasics)
setwd("~/Documents/default/Computational_Finance/DAX_data/")
dax <- read.csv("dax.csv")

# Compute Returns
alvR <- 1:252
bmwR <- 1:252
cbkR <- 1:252
tkaR <- 1:252

for (i in 1:252){
  alvR[i] <- dax$alvAC[i] / dax$alvAC[i + 1] - 1
  bmwR[i] <- dax$bmwAC[i] / dax$bmwAC[i + 1] - 1
  cbkR[i] <- dax$cbkAC[i] / dax$cbkAC[i + 1] - 1
  tkaR[i] <- dax$tkaAC[i] / dax$tkaAC[i + 1] - 1
}

# Create Returns Table
daxR <- data.frame(dax$date, alvR, bmwR, cbkR, tkaR)

# Compute Log Returns
daxRlog <- log(daxR[2:5] + 1)

# Plot Returns and Log Returns
plot(dax$date, daxRlog$alvR, type = "l", xlab = "Dates", ylab = "Returns")
lines(dax$date, daxRlog$alvR, type = "l", col = "red")

# Basic Statistics
basicStats(na.omit(daxRlog))

# Box Plots
boxplot(daxR[, 2:5], horizontal = TRUE)

# Histogram Showing Normality
hist(alvR, probability = T, xlab = "ALV.DE Returns", main = NULL)

# Ex_2_4
alv <- na.omit(alvR)
DS <- density(na.omit(alvR))
yl = c(min(DS$y), max(DS$y))
hist(alv, probability = T, xlab = "ALV Returns", main = NULL, ylim = yl)
rug(alv)              
lines(DS)
a = seq(min(alv), max(alv), 0.001)
# Uncomment below for Dotted Line Distribution
# points(a, dnorm(a, mean(alv), sd(alv)), type = "l", lty = 2)
lines(a, dnorm(a, mean(alv), sd(alv)), col = "red")

# Test for Normality (Distribution)
shapiro.test(alv)

# Covariance Evaluation
cov(daxRlog, use = "complete.obs")