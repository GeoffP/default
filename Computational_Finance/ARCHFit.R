library(fGarch)
setwd("~/Documents/default/Computational_Finance/DAX_data/")
daxRl = read.table("daxRlog", header = T)
alvRl = na.omit(daxRl$alvR)
alvRl2 <- alvRl^2
acf(alvRl2)
pacf(alvRl2)
arch5 = garchFit(formula = ~garch(5, 0), data = alvRl, cond.dist = "norm")
summary(arch5)

# Graphical Diagnosis
plot(arch5) # select option 1
lines(alvRl, col = "blue")

# Predicting Five Steps Ahead And Graphing Output
predict(arch5, 7, plot = TRUE)