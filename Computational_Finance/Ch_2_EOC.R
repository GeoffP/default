library(quantmod)

# Get Data
aapl = getSymbols("AAPL", src = "yahoo", auto.assign = FALSE)

# Calculate Daily Returns
apRd = periodReturn(aapl, period = "daily", type = "log")
dsd = density(apRd)
yl = c(min(dsd$y), max(dsd$y))
plot(dsd, main = NULL, ylim = yl)
a = seq(min(apRd), max(apRd), 0.001)
points(a, dnorm(a, mean(apRd), sd(apRd)), type = "l", lty = 2)

# Calculate Weekly Returns
apRw = periodReturn(aapl, period = "weekly", type = "log")
dsd = density(apRw)
yl = c(min(dsd$y), max(dsd$y))
plot(dsd, main = NULL, ylim = yl)
a = seq(min(apRw), max(apRw), 0.001)
points(a, dnorm(a, mean(apRw), sd(apRw)), type = "l", lty = 2)

# Calculate Monthly Returns
apRm = periodReturn(aapl, period = "monthly", type = "log")
dsd = density(apRm)
yl = c(min(dsd$y), max(dsd$y))
plot(dsd, main = NULL, ylim = yl)
a = seq(min(apRm), max(apRm), 0.001)
points(a, dnorm(a, mean(apRm), sd(apRm)), type = "l", lty = 2)