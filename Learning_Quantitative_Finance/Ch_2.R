# Set Our Working Director
setwd("~/Documents/default/Learning_Quantitative_Finance/")
Sampledata <- read.csv("DataChap2.csv")

# Plot The Height Of The Normal Distribution
y <- dnorm(Sampledata$Return, mean = mean(Sampledata$Return),
           sd = sd(Sampledata$Return))
plot(Sampledata$Return, y)

# Using pnorm To Find The Probability Of A Result Less Than x (x = 0.02 [2%] here)
1 - pnorm(0.02, mean = mean(Sampledata$Return), sd = sd(Sampledata$Return, na.rm = FALSE))

# Using a Probability to Seek an Actual Value using qnorm
qnorm(0.159837, mean = mean(Sampledata$Return), sd = +sd(Sampledata$Return, na.rm = FALSE),
      lower.tail = FALSE)

# Create Five Random Numbers Using The Mean and Standard Deviaion of Our Sample
rnorm(5, mean = mean(Sampledata$Return), sd = sd(Sampledata$Return, na.rm = FALSE))

# Finding The Density of a Lognormal Function Using dlnorm
y <- dlnorm(Sampledata$Volume, meanlog = mean(Sampledata$Volume),
            sdlog = sd(Sampledata$Volume, na.rm = FALSE))
plot(Sampledata$Volume, y)

# Finding the Cumulative Density Function of a Lognormal Distribution
y <- plnorm(Sampledata$Volume, meanlog = mean(Sampledata$Volume),
            sdlog = sd(Sampledata$Volume, na.rm = FALSE))
plot(Sampledata$Volume, y)

# qlnorm [Unsure if Accurate]
y <- qlnorm(c(seq(0.0, 0.9, by = 0.1)), meanlog = mean(log(Sampledata$Volume)),
       sdlog = log(sd(Sampledata$Volume, na.rm = FALSE)), lower.tail = TRUE, log.p = FALSE)
plot(seq(0.0, 0.9, by = 0.1), y)

# rlnorm [Unsure if Accurate]
rlnorm(10, meanlog = mean(log(Sampledata$Volume)), sdlog = (log(sd(Sampledata$Volume,
                                                                   na.rm = FALSE))))

# Poisson Distribution ## Lambda is mean occurances per interval
# x = # of occurances per interval [Upper-Tail]
ppois(15, lambda = 10)

# Poisson Distribution ## " [Lower-Tail]
ppois(15, lambda = 10, lower.tail = FALSE)

# Generate 10 Random Numbers Between 1 and 5
runif(10, min = 1, max = 5)

# Generalized Extreme Value Theory
library(POT)
data(ardieres)
abc <- ardieres[1:10000, ]
events <- clust(abc, u = 1.5, tim.cond = 8 / 365, clust.max = TRUE)
par(mfrow = c(2, 2))
mrlplot(events[, "obs"])
diplot(events)
tcplot(events[, "obs"], which = 1)
tcplot(events[, "obs"], which = 2)

# Evaluation and GPD Model Fit
obs <- events[, "obs"]
ModelFit <- fitgpd(obs, thresh = 5, "pwmu")
ModelFit

# RESET PLOT: NOT IMPORTANT GENERALLY
par(mfrow = c(1,1))

# Random Sampling
# Without Replacement
RandomSample <- Sampledata[sample(1:nrow(Sampledata), 
                                  10, replace = FALSE),]
# With Replacement
RandomSample <- Sampledata[sample(1:nrow(Sampledata), 
                                  10, replace = TRUE),]

# Stratified Sampling
library(sampling)
table(Sampledata$Flag, Sampledata$Sentiments)
### NOT WORKING
#Stratsubset = strata(test, stratanames = c("Flag", "Sentiments"),
#                     size = c(6, 5, 4, 3), method = "srswor")
#Stratsubset

# Mean
mean(Sampledata$Volume)

# Median
median(Sampledata$Volume)

# Mode
findmode <- function(x){
  uniqx <- unique(x)
  uniqx[which.max(tabulate(match(x, uniqx)))]
}
findmode(Sampledata$Return)

# Summary Statistics
summary(Sampledata$Volume)

# Moment
library(e1071)
moment(Sampledata$Volume, order = 3, center = TRUE)

# Kurtosis
kurtosis(Sampledata$Volume)

# Skewness
skewness(Sampledata$Volume)

# Correlation
library(Hmisc)
x <- as.matrix(Sampledata[, 2:5])
cor(x)
rcorr(x, type = "pearson")

# AutoCorrelation / Serial Correlation
acf(Sampledata$Volume)

# Partial AutoCorrelation
pacf(Sampledata$Volume)

# Cross-Correlation Function
ccf(Sampledata$Volume, Sampledata$High, main = "CCF Plot")

# Lower Tail Test of Population Mean with Known Variance
xbar = 9.9 # Sample Mean
mu0 = 10 # Hypothesized Value
sig = 1.1 # Standard Deviation of Population
n = 30 # Sample Size
z = (xbar - mu0) / (sig / sqrt(n)) # Test Statistic
z

alpha = 0.05 # Signficance Level
z.alpha = qnorm(1 - alpha)
-z.alpha # Test Statistic Is Greater Than Critical Value REJECTED
pnorm(z) # Alternate p-value test

# Upper Tail Test of Population Mean with Known Variance
xbar = 5.1
mu0 = 5
sig = 0.25
n = 30
z = (xbar - mu0) / (sig / sqrt(n))
z # Test Statistic

alpha = 0.05
z.alpha = qnorm(1 - alpha)
z.alpha # Critical Value

# Critical Value Is Less Than Test Statistic ## Reject NULL Hypothesis
pnorm(z, lower.tail = FALSE) # 0.014 < 0.05 NULL Is Rejected

# Two-Tailed Test of Population Mean with Known Variance
xbar = 1.5
mu0 = 2
sig = 0.1
n = 30
z = (xbar - mu0) / (sig / sqrt(n))
z

# Test Statistic
alpha = 0.05
z.half.alpha = qnorm(1 - alpha / 2)  
c(-z.half.alpha, z.half.alpha) # Upper and Lower Confidence Intervals

2* pnorm(z) # p-value

# Lower-Tail Test of Population Mean With Unknown Variance
xbar = 0.9
mu0 = 1
sig = 0.1
n = 30
t = (xbar - mu0) / (sig / sqrt(n))
t

# Significance Level
alpha = 0.05
t.alpha = qt(1 - alpha, df = n - 1)
-t.alpha
pt(t, df = n - 1) # p-Value

# Upper Tail Test of Population Mean with Unknown Variance
xbar = 3.1
mu0 = 3
sig = 0.2
n = 30
t = (xbar - mu0) / (sig / sqrt(n))
t

# Significance Level
alpha = 0.05
t.alpha = qt(1 - alpha, df = n - 1)
t.alpha
pt(t, df = n -1, lower.tail = FALSE) # p-Value

# Two-Tailed Test of Population Mean with Unknown Variance
xbar = 1.9
mu0 = 2
sig = 0.1
n = 30
t = (xbar - mu0) / (sig / sqrt(n))
t

# Test Statistic
alpha = 0.05
t.half.alpha = qt(1 - alpha / 2, df = n - 1)
t.half.alpha
c(-t.half.alpha, t.half.alpha)

### Maximimum Likelihood Estimation (MLE)

set.seed(100)
NO_values <- 100
Y <- rnorm(NO_values, mean = 5, sd = 1)
mean(Y)
sd(Y)

# Log Likelihood Function
LogL <- function(mu, sigma) {
  A = dnorm(Y, mu, sigma)
  -sum(log(A))
}

# Apply MLE Function Log Likelihood
library(stats4)
mle(minuslogl = LogL, start = list(mu = 2, sigma = 2), method = "L-BFGS-B",
    lower = c(-Inf, 0), upper = c(Inf, Inf))

## Linear Regression
Y <- Sampledata$Adj.Close
X <- Sampledata$Volume
fit <- lm(Y ~ X)
summary(fit)

## Outliers

# Boxplot
boxplot(Sampledata$Volume, horizontal = TRUE, main = "Volume",
        boxwex = 0.1)

# Local Outlier Factor (LOF)
library(DMwR)
Sampledata1 <- Sampledata[, 2:4]
outlier.scores <- lofactor(Sampledata1, k = 4) # Number of Neighbors Compared
plot(density(outlier.scores))

# Obtain Top Five Outliers
order(outlier.scores, decreasing = T)[1:5]

## Standardization
# Center The Data In Dataset
scale(Sampledata$Volume, center = TRUE, scale = FALSE)

# Center And Standardize Data in Dataset
scale(Sampledata$Volume, center = TRUE, scale = TRUE)

# Normalization
normalized = (Sampledata$Volume - min(Sampledata$Volume)) / 
  (max(Sampledata$Volume) - min(Sampledata$Volume))
normalized