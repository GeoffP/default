############
# ARIMA Modeling with R
# SOURCE: https://www.datascience.com/blog/introduction-to-forecasting-with-arima-in-r-learn-data-science-tutorials
#
setwd("~/Documents/default/")

library(ggplot2)
library(forecast)
library(tseries)

daily_data = read.csv('day.csv', header = TRUE, stringsAsFactors = FALSE)
daily_data$Date = as.Date(daily_data$dteday)

ggplot(daily_data, aes(Date, cnt)) +
  geom_line() +
  scale_x_date('month') +
  ylab("Daily Bike Checkouts") +
  xlab("")

count_ts = ts(daily_data[, c('cnt')])

daily_data$clean_cnt = tsclean(count_ts)

ggplot() +
  geom_line(data = daily_data, aes(x = Date, y = clean_cnt)) +
  ylab('Cleaned Bicycle Count')

# Using Clean Count with NO Outliers
daily_data$cnt_ma = ma(daily_data$clean_cnt, order = 7)
daily_data$cnt_ma30 = ma(daily_data$clean_cnt, order = 30)

ggplot() +
  geom_line(data = daily_data, aes(x = Date, y = clean_cnt, colour = "Counts")) +
  geom_line(data = daily_data, aes(x = Date, y = cnt_ma, colour = "Weekly Moving Average")) +
  geom_line(data = daily_data, aes(x = Date, y = cnt_ma30, colour = "Monthly Moving Average")) +
  ylab('Bicycle Count')

# Check for Seasonality
count_ma = ts(na.omit(daily_data$cnt_ma), frequency = 30)
decomp = stl(count_ma, s.window = "periodic")
deseasonal_cnt <- seasadj(decomp)
plot(decomp)

# ADF Test For Stationarity
#
# The augmented Dickey-Fuller (ADF) test is a formal statistical test for stationarity.
# The null hypothesis assumes that the series is non-stationary.
# ADF procedure tests whether the change in Y can be explained by lagged value and a linear trend.
# If contribution of the lagged value to the change in Y is non-significant and there is a presence
# of a trend component, the series is non-stationary and null hypothesis will not be rejected.
#

adf.test(count_ma, alternative = "stationary")

# The number of differences performed is represented by the d component of ARIMA.
# Now, we move on to diagnostics that can help determine the order of differencing.

# ACF plots display correlation between a series and its lags.
# In addition to suggesting the order of differencing, ACF plots can help in determining the order
# of the M A (q) model.

Acf(count_ma, main = '')

# Partial autocorrelation plots (PACF), as the name suggests,
# display correlation between a variable and its lags that is not explained by previous lags.
# PACF plots are useful when determining the order of the AR(p) model.

Pacf(count_ma, main = '')

# ACF shows substantial correlation for many lags. Pacf only shows spikes on 1 and 7. We
# decide to difference at 1 lag and evaluate further. (d = 1)

count_d1 = diff(deseasonal_cnt, differences = 1)
plot(count_d1)
adf.test(count_d1, alternative = "stationary")

# Next, spikes at particular lags of the differenced series
# can help inform the choice of p or q for our model:

Acf(count_d1, main = "ACF for Differenced Series")
Pacf(count_d1, main = "PACF for Differenced Series")

# Now let's fit a model. The forecast package allows the user to explicitly specify the order
# of the model using the arima() function, or automatically generate a set of optimal (p, d, q)
# using auto.arima(). This function searches through combinations of order parameters and
# picks the set that optimizes model fit criteria.

# There exist a number of such criteria for comparing quality of fit across multiple models.
# Two of the most widely used are Akaike information criteria (AIC) and Baysian information criteria
# (BIC). These criteria are closely related and can be interpreted as an estimate of how much
# information would be lost if a given model is chosen. When comparing models,
# one wants to minimize AIC and BIC.

# While auto.arima() can be very useful, it is still important to complete steps 1-5
# in order to understand the series and interpret model results.
# Note that auto.arima() also allows the user to specify maximum order for (p, d, q),
# which is set to 5 by default.

auto.arima(deseasonal_cnt, seasonal = FALSE)

# So now we have fitted a model that can produce a forecast,
# but does it make sense? Can we trust this model?
# We can start by examining ACF and PACF plots for model residuals.
# If model order parameters and structure are correctly specified,
# we would expect no significant autocorrelations present. 

fit <- auto.arima(deseasonal_cnt, seasonal = FALSE)
tsdisplay(residuals(fit), lag.max = 45, main = '(1, 1, 1) Model Residuals')

# There is a clear pattern present in ACF/PACF and model residuals plots repeating at lag 7.
# This suggests that our model may be better off with a different specification,
# such as p = 7 or q = 7. 

# We can repeat the fitting process allowing for the MA(7) component
# and examine diagnostic plots again. 

fit2 = arima(deseasonal_cnt, order = c(1, 1, 7))
fit2
tsdisplay(residuals(fit2), lag.max = 15, main = 'Seasonal Model Residuals')

# Forecasting using a fitted model is straightforward in R.
# We can specify forecast horizon h periods ahead for predictions to be made,
# and use the fitted model to generate those predictions:

fcast <- forecast(fit2, h = 30)
plot(fcast)

# The light blue line above shows the fit provided by the model,
# but what if we wanted to get a sense of how the model will perform in the future?
# One method is to reserve a portion of our data as a "hold-out" set, fit the model,
# and then compare the forecast to the actual observed values:

hold <- window(ts(deseasonal_cnt), start = 700)

fit_no_holdout = arima(ts(deseasonal_cnt[-c(700:725)]), order = c(1, 1, 7))

fcast_no_holdout <- forecast(fit_no_holdout, h = 25)
plot(fcast_no_holdout, main = "")
lines(ts(deseasonal_cnt))

# How can we improve the forecast and iterate on this model?
# One simple change is to add back the seasonal component we extracted earlier.
# Another approach would be to allow for (P, D, Q) components to be included to the model,
# which is a default in the auto.arima() function.
# Re-fitting the model on the same data, we see that there still might be some seasonal pattern
# in the series, with the seasonal component described by AR(1):

fit_w_seasonality = auto.arima(deseasonal_cnt, seasonal = TRUE)
fit_w_seasonality

# Note that (p,d,q) parameters also changed after we included a seasonal component.
# We can go through the same process of evaluating model residuals and ACF/PACF plots and
# adjusting the structure if necessary. For example, we notice the same evidence of higher order
# is present in the auto correlations with lag 7, which suggests that a higher-order component
# might be needed:

seas_fcast <- forecast(fit_w_seasonality, h = 30)
plot(seas_fcast)
tsdisplay(residuals(seas_fcast), lag.max = 15, main = 'Seasonal Model Residuals')

############
############
### EOF ####
############
